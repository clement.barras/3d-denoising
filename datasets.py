import json
from os.path import join
from typing import Union

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
from torch.utils import data

from settings import SUMMARY_DF, EXTRACTION_FOLDER
from utils import folder_name, translate_array, rotate_array, str_to_tuple


class MCDataset(data.Dataset):

    def __init__(self, config: dict, device: str, summary_df: Union[str, pd.DataFrame] = SUMMARY_DF,
                 data_folder: str = EXTRACTION_FOLDER):
        """

        :param config: dictionarry containing configuration
        :param device: torch device: can be cpu or cuda
        :param summary_df: dataframe, or path to the dataframe which contains metadata for each slice of each volume.
        Must be calculated beforehand with the function create_summary_df
        :param data_folder: directory containing the volumes
        """
        if isinstance(summary_df, str):  # open the metadata table
            summary_df = pd.read_csv(summary_df)
        pd.options.mode.chained_assignment = None  # disable warnings when chain assign on pandas DataFrame
        self.device = device
        self.n_in = config["n_in"]  # number of particles of the input images
        self.n_out = config["n_out"]
        self.patients = config["patients"]  # patients to keep in the data set
        self.filter = config["filter"]  # if the dataset is filtered, keeping only the images containing the ray
        self.normalize = config["normalize"]  # how to normalize the ray power
        self.rotate = config["rotate"]  # if to rotate/box
        self.boxing = config["boxing"]
        self.data_folder = data_folder
        # Underlying df contains references to each image/volume along with their metadata, to ensure a very fast
        # extraction of the data (see __get_item__)
        self.df = self._create_df(summary_df, self.n_in, self.n_out, self.patients)
        print("Dataset created on device {}".format(device))

    def _create_df(self, summary_df, n_in, n_out, patients):
        """
            Initialize the underlying dataframe
        """
        df_in = summary_df.loc[summary_df["n_particles"] == n_in]  # filtered dataframe for input data
        df_out = summary_df.loc[summary_df["n_particles"] == n_out]  # filtered dataframe for output data
        # we merge them to keep volumes which share the same characteristics
        df_in.merge(df_out, on=["anatomy", "patient", "field_size", "angle", "type", "slice"], suffixes=("_in", "_out"))
        df_in = df_in.loc[df_in["anatomy"].isin(patients.keys())]  # filter anatomies
        for anatomy, patient_list in patients.items():
            df_in = df_in.loc[~((df_in["anatomy"] == anatomy) & ~(df_in["patient"].isin(patient_list)))]
            for patient in patient_list:
                patient_indices = (df_in["anatomy"] == anatomy) & (df_in["patient"] == patient)
                volume_folder = join(self.data_folder, anatomy, "patient{}".format(patient))
                with open(join(volume_folder, "metadata.json"), "r") as fp:  # open volume's metadata
                    dic = json.load(fp)
                for k, v in dic.items():
                    for j, suf in enumerate(["_x", "_y", "_z"]):
                        df_in.loc[patient_indices, k + suf] = v[j]
        df_in["volume_id"] = \
            df_in.groupby(["anatomy", "patient", "n_particles", "angle", "field_size"], axis=0).grouper.group_info[0]
        ### FILTERING OR PER VOLUME PROCESSING
        if self.filter:  # if filtering, get ray coordinates an keep slices where the ray is present
            df_in["to_filter"] = False
            for vol in df_in["volume_id"].unique():
                volume_indices = df_in["volume_id"] == vol
                ray_coords = self.get_ray_coordinates(df_in[volume_indices].iloc[0])
                df_in["to_filter"] = df_in["to_filter"] | (volume_indices & df_in["slice"].isin(ray_coords["slices"]))
            df_in = df_in.loc[df_in["to_filter"]]
            df_in.drop(columns=["to_filter", "n_particles"], inplace=True)
        return df_in

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        """
        Return input and target tensors along with the rays average energy and its devitation.
        """
        item = self.df.iloc[index].copy(deep=True)
        # get the data tensors (and applying operations in specified in config)
        input_dose = self._get_dose_array(item, self.n_in)[np.newaxis]
        output_dose = self._get_dose_array(item, self.n_out)[np.newaxis]
        patient = self._get_patient_array(item)[np.newaxis]
        patient = (patient - np.min(patient)) / (np.max(patient) - np.min(patient))  # norm to [0, 1]
        r = self.n_out / self.n_in
        if self.normalize == "image":  # normalize on the fly, very fast
            m_in = np.mean(input_dose, axis=(1, 2))[:, None, None]
            s_in = np.std(input_dose, axis=(1, 2))[:, None, None]
            input_dose = (input_dose - m_in) / s_in
            output_dose = (output_dose / r - m_in) / s_in
            m_in = torch.FloatTensor(m_in).to(self.device)
            s_in = torch.FloatTensor(s_in).to(self.device)
        input_dose = torch.squeeze(torch.FloatTensor(input_dose), 0).to(self.device)
        output_dose = torch.squeeze(torch.FloatTensor(output_dose), 0).to(self.device)
        patient = torch.squeeze(torch.FloatTensor(patient), 0).to(self.device)
        return torch.stack((input_dose, patient)), output_dose, m_in, s_in

    def _get_dose_array(self, item, n_particles=None):
        """
        Loads a numpy array containing a given ray volume (designated by item)
        """
        if n_particles is not None:
            item.loc["n_particles"] = n_particles
        folder = folder_name(item, self.data_folder)
        slice_name = "slice_#{}.npy".format(item["slice"])
        dose_ary = np.load(join(folder, slice_name))
        if self.rotate:  # see utils/transforms.py for info about translation and rotation
            coords = self._get_ray_coordinates(item)
            new_center = (coords["center_x"], coords["center_y"])
            dose_ary = translate_array(dose_ary, new_center=new_center)
            dose_ary = rotate_array(dose_ary, -item["angle"])
        return dose_ary

    def _get_patient_array(self, item):
        """
        Loads a numpy array containing a given patient CT volume (designated by item)
        """
        patient_folder = join(self.data_folder, item["anatomy"], "patient{}".format(item["patient"]), "CT")
        slice_name = "slice_#{}.npy".format(item["slice"])
        patient_ary = np.load(join(patient_folder, slice_name))
        if self.rotate:  # see utils/transforms.py for info about translation and rotation
            coords = self._get_ray_coordinates(item)
            new_center = (coords["center_x"], coords["center_y"])
            patient_ary = translate_array(patient_ary, new_center=new_center)
            patient_ary = rotate_array(patient_ary, -item["angle"])
        if self.boxing:
            margin = int(256 * item["field_size"] / 50)
            patient_ary[:, :256 - margin] = 0
            patient_ary[:, 256 + margin:] = 0
        return patient_ary

    def _get_ray_coordinates(self, item):
        """
        Calculate rays parameters from volume metadata
        """
        iso = str_to_tuple(item["iso"])
        center_x = (-item["volume_origin_x"] + iso[0]) / item["volume_spacing_x"]
        center_y = (-item["volume_origin_y"] + iso[1]) / item["volume_spacing_y"]
        center_z = (-item["volume_origin_z"] + iso[2]) / item["volume_spacing_z"]
        depth = np.ceil(1.5 * item["field_size"] / item["volume_spacing_z"])
        slices = list(np.arange(center_z - depth, center_z + depth + 1, dtype=np.int))
        coords = {"center_x": center_x,
                  "center_y": center_y,
                  "center_z": center_z,
                  "depth": depth,
                  "slices": slices}
        return coords

    def plot_volume(self, volume_id, n_particles):
        """
        Convenience function to visualize a specified volume. It will be represented as a series of 2D plots.
        """
        volume_df = self.df.loc[self.df["volume_id"] == volume_id]
        ray_coords = self._get_ray_coordinates(volume_df.iloc[0])
        print(ray_coords["slices"], ray_coords["center_x"], ray_coords["center_y"])
        # volume_df = volume_df.loc[self.df["slice"].isin(ray_coords["slices"])]
        volume_df = volume_df.sort_values(by="slice")
        n_fig = len(volume_df)
        print(n_fig)
        fig, ax = plt.subplots(int(np.ceil(n_fig // 4) + 1), 4, figsize=(18, 1.5 * n_fig))
        for i in range(n_fig):
            item = volume_df.iloc[i].copy(deep=True)
            if not (i):  ###
                print(item)  ###
            dose = self._get_dose_array(item, n_particles)
            # dose[256, 256] = 1.5*np.max(dose)
            patient = self._get_patient_array(item)
            ax[i // 4][i % 4].imshow(patient, cmap="bone")
            ax[i // 4][i % 4].imshow(dose, alpha=0.6, cmap="hot")
            ax[i // 4][i % 4].set_title("Slice #{}, Energy: {:.3e}".format(i, np.sqrt(np.mean(dose ** 2))))
        for i in range(n_fig, n_fig + 4 - n_fig % 4):
            ax[i // 4][i % 4].imshow(np.zeros((1, 1)))


class MCLSTMDataset(MCDataset):

    def create_df(self, summary_df, patients):

        # consider now a different dataframe for each possible n_particles
        # and merge them to keep only volumes for which ray of all n_particles exist
        df_1 = summary_df.loc[summary_df["n_particles"] == 1e5]
        df_2 = summary_df.loc[summary_df["n_particles"] == 1e6]
        df_3 = summary_df.loc[summary_df["n_particles"] == 1e7]
        df_4 = summary_df.loc[summary_df["n_particles"] == 1e8]

        df_out = summary_df.loc[summary_df["n_particles"] == 1e9]
        df_1.merge(df_2, on=["anatomy", "patient", "n_particles", "field_size", "angle", "type", "slice"],
                   suffixes=("_1", "_2"))
        df_1.merge(df_3, on=["anatomy", "patient", "n_particles", "field_size", "angle", "type", "slice"],
                   suffixes=("", "_3"))
        df_1.merge(df_4, on=["anatomy", "patient", "n_particles", "field_size", "angle", "type", "slice"],
                   suffixes=("", "_4"))
        df_1.merge(df_out, on=["anatomy", "patient", "n_particles", "field_size", "angle", "type", "slice"],
                   suffixes=("", "_out"))
        df_1 = df_1.loc[df_1["anatomy"].isin(patients.keys())]
        for anatomy, patient_list in patients.items():
            df_1 = df_1.loc[~((df_1["anatomy"] == anatomy) & ~(df_1["patient"].isin(patient_list)))]
            for patient in patient_list:
                patient_indices = (df_1["anatomy"] == anatomy) & (df_1["patient"] == patient)
                volume_folder = join(self.data_folder, anatomy, "patient{}".format(patient))
                with open(join(volume_folder, "metadata.json"), "r") as fp:
                    dic = json.load(fp)  # open volume's metadata
                for k, v in dic.items():
                    for j, suf in enumerate(["_x", "_y", "_z"]):
                        df_1.loc[patient_indices, k + suf] = v[j]
        df_1["volume_id"] = \
            df_1.groupby(["anatomy", "patient", "n_particles", "angle", "field_size"], axis=0).grouper.group_info[0]
        ### FILTERING OR PER VOLUME PROCESSING
        if self.filter:  # if filtering, get ray coordinates an keep slices where the ray is present
            df_1["to_filter"] = False
            for vol in df_1["volume_id"].unique():
                volume_indices = df_1["volume_id"] == vol
                ray_coords = self.get_ray_coordinates(df_1[volume_indices].iloc[0])
                df_1["to_filter"] = df_1["to_filter"] | (volume_indices & df_1["slice"].isin(ray_coords["slices"]))
            df_1 = df_1.loc[df_1["to_filter"]]
            df_1.drop(columns=["to_filter", "n_particles"], inplace=True)
        return df_1

    def __getitem__(self, index):
        """
        Similar to MCDataset's one, but here are output all versions of the ray instead of just two.
        Rays are normalized accordingly depending of the number of particles.
        """
        item = self.df.iloc[index].copy(deep=True)
        input_1 = self.get_dose_array(item, 1e5)[np.newaxis]
        input_2 = self.get_dose_array(item, 1e6)[np.newaxis]
        input_3 = self.get_dose_array(item, 1e7)[np.newaxis]
        input_4 = self.get_dose_array(item, 1e8)[np.newaxis]
        output_dose = self.get_dose_array(item, 1e9)[np.newaxis]
        patient = self.get_patient_array(item)[np.newaxis]
        patient = (patient - np.min(patient)) / (np.max(patient) - np.min(patient))
        m_in = 0
        s_in = 0

        if self.normalize == "image":
            m_in = np.mean(input_4, axis=(1, 2))[:, None, None]
            s_in = np.std(input_4, axis=(1, 2))[:, None, None]
            input_4 = (input_4 - m_in) / s_in
            input_3 = (input_3 * 10 - m_in) / s_in
            input_2 = (input_2 * 100 - m_in) / s_in
            input_1 = (input_1 * 1000 - m_in) / s_in
            output_dose = (output_dose / 10 - m_in) / s_in
            m_in = torch.FloatTensor(m_in).to(self.device)
            s_in = torch.FloatTensor(s_in).to(self.device)
        input_1 = torch.squeeze(torch.FloatTensor(input_1), 0).to(self.device)
        input_2 = torch.squeeze(torch.FloatTensor(input_2), 0).to(self.device)
        input_3 = torch.squeeze(torch.FloatTensor(input_3), 0).to(self.device)
        input_4 = torch.squeeze(torch.FloatTensor(input_4), 0).to(self.device)
        output_dose = torch.squeeze(torch.FloatTensor(output_dose), 0).to(self.device)
        patient = torch.squeeze(torch.FloatTensor(patient), 0).to(self.device)
        inputs = torch.stack((torch.stack((input_1, patient)), torch.stack((input_2, patient)),
                              torch.stack((input_3, patient)), torch.stack((input_4, patient))))
        # inputs = torch.stack((input_1.unsqueeze(0), input_2.unsqueeze(0), input_3.unsqueeze(0), input_4.unsqueeze(0)))
        return inputs, output_dose, m_in, s_in
