SUMMARY_DF = "../../data/extracted/summary.csv"  # Summarizes the metadata of each volumes in one file

DOSE_FOLDER = "../../data/denoising/output/"  # ray volumes
PATIENT_FOLDER = "../../data/denoising/data/"  # patient volumes
EXTRACTION_FOLDER = "../../data/extracted/"  # preprocessed npy ary containing extracted volumes.
