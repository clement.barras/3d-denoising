import argparse
import os
import subprocess
from io import BytesIO

import numpy as np
import pandas as pd
import torch
import torch.optim as optim
from torch.utils import data

from datasets import MCLSTMDataset
from models.lstm import LSTM_MC
from utils import train, test


def get_free_gpu(): # returns the id of the GPU with the largest available free memory
    gpu_stats = subprocess.check_output(["nvidia-smi", "--format=csv", "--query-gpu=memory.used,memory.free"])
    gpu_df = pd.read_csv(BytesIO(gpu_stats),
                         names=['memory.used', 'memory.free'],
                         skiprows=1)
    print('GPU usage:\n{}'.format(gpu_df))
    gpu_df['memory.free'] = gpu_df['memory.free'].map(lambda x: float(x.rstrip(' [MiB]')))
    idx = gpu_df['memory.free'].idxmax()
    print('Using GPU{} with {} free MiB'.format(idx, gpu_df.iloc[idx]['memory.free']))
    return idx


# Training settings
parser = argparse.ArgumentParser(description='Monte carlo training script')
parser.add_argument('--data', type=str, default='5to8', metavar='D',
                    help="folder where data is located. train_images/ and val_images/ need to be found in the folder")
parser.add_argument('--batch-size', type=int, default=64, metavar='B',
                    help='input batch size for training (default: 64)')
parser.add_argument('--epochs', type=int, default=10, metavar='N',
                    help='number of epochs to train (default: 10)')
parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                    help='learning rate (default: 0.01)')
parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                    help='SGD momentum (default: 0.5)')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                    help='how many batches to wait before logging training status')
parser.add_argument('--experiment', type=str, default='experiment', metavar='E',
                    help='folder where experiment outputs are located.')
parser.add_argument('--normalize', type=str, default='none', metavar='E',
                    help='Type of normalization to apply.')

args = parser.parse_args()
use_cuda = torch.cuda.is_available()
torch.manual_seed(args.seed)

# Create experiment folder
experiment_folder = os.path.join('../saved_models', args.experiment)
if not os.path.isdir(experiment_folder):
    os.makedirs(experiment_folder)

N_IN = 1e5
N_OUT = 1e9
NORMALIZE = 'image'
FILTER = True
ROTATE = True
BATCH_SIZE = 4
DEVICE = torch.device(f"cuda:{get_free_gpu()}" if torch.cuda.is_available() else "cpu")

train_config = {"normalize": NORMALIZE, "filter": FILTER, "rotate": ROTATE,
                "patients": {"lung": [1, 2, 3, 4]}}

test_config = {"normalize": NORMALIZE, "filter": FILTER, "rotate": ROTATE,
               "patients": {"lung": [5]}}

train_dataset = MCLSTMDataset(train_config, DEVICE)
test_dataset = MCLSTMDataset(test_config, DEVICE)

train_loader = data.DataLoader(train_dataset, batch_size=BATCH_SIZE, shuffle=True)
test_loader = data.DataLoader(test_dataset, batch_size=BATCH_SIZE, shuffle=True)

LR = 0.01
network = LSTM_MC(channels=1, embedding_dim=65536, batch_size=BATCH_SIZE).to(DEVICE)
optimizer = optim.Adam(network.parameters(), lr=LR)
epochs = 50

for epoch in range(1, epochs):
    print(optimizer.state_dict()['param_groups'][0]['lr'])
    train(train_loader, epoch, network, optimizer)
    test(test_loader, network)
    optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=np.sqrt(.1), patience=3)
    model_file = args.experiment + '/model_' + str(epoch) + '.pth'
    path_save = os.path.join('../saved_models', model_file)
    torch.save(network.state_dict(), path_save)
    print('\nSaved model to ' + path_save + '.')
