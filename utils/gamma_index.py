import numpy as np
import pymedphys
from pymedphys.gamma import gamma_shell


# Functions to calculate gamma_index, a score used in radio therapy to
# compare 2 dose (ray energy) distributions.

def get_gamma_index(dose_reference, dose_evaluation, gamma_options):
    x_spacing = gamma_options["x_spacing"]
    y_spacing = gamma_options["y_spacing"]
    n = dose_reference.shape[0]
    p = dose_reference.shape[1]

    x_axis = np.array([x_spacing * i for i in range(p)])
    y_axis = np.array([y_spacing * i for i in range(n)])
    coords_reference = (y_axis, x_axis)
    coords_evaluation = (y_axis, x_axis)

    distance_mm_threshold = gamma_options["distance_mm_threshold"]
    dose_percent_threshold = gamma_options["dose_percent_threshold"]
    lower_percent_dose_cutoff = gamma_options["lower_percent_dose_cutoff"]
    interp_fraction = gamma_options["interp_fraction"]

    gamma = pymedphys.gamma.gamma_shell(coords_reference, dose_reference,
                                        coords_evaluation, dose_evaluation,
                                        dose_percent_threshold, distance_mm_threshold,
                                        lower_percent_dose_cutoff,
                                        interp_fraction,
                                        max_gamma=2)

    return gamma


def get_gamma_index_3D(dose_reference, dose_evaluation, gamma_options):
    x_spacing = gamma_options["x_spacing"]
    y_spacing = gamma_options["y_spacing"]
    z_spacing = gamma_options["z_spacing"]
    m = dose_reference.shape[0]
    n = dose_reference.shape[1]
    p = dose_reference.shape[2]

    x_axis = np.array([x_spacing * i for i in range(p)])
    y_axis = np.array([y_spacing * i for i in range(n)])
    z_axis = np.array([z_spacing * i for i in range(m)])
    coords_reference = (z_axis, y_axis, x_axis)
    coords_evaluation = (z_axis, y_axis, x_axis)

    distance_mm_threshold = gamma_options["distance_mm_threshold"]
    dose_percent_threshold = gamma_options["dose_percent_threshold"]
    lower_percent_dose_cutoff = gamma_options["lower_percent_dose_cutoff"]
    interp_fraction = gamma_options["interp_fraction"]

    gamma = pymedphys.gamma.gamma_shell(coords_reference, dose_reference,
                                        coords_evaluation, dose_evaluation,
                                        dose_percent_threshold, distance_mm_threshold,
                                        lower_percent_dose_cutoff,
                                        interp_fraction,
                                        max_gamma=2)

    return gamma
