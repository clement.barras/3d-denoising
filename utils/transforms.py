import numpy as np
from PIL import Image


def rotate_array(ar, angle, resample=Image.BILINEAR):
    # If 2D array, rotation is easy with PIL
    # The minimum value of the array remains the same as before
    if ar.ndim == 2:
        min_ar = np.min(ar)
        ar = ar - min_ar  # min(ar) becomes 0
        im = Image.fromarray(ar - min_ar)
        rot_im = im.rotate(angle, resample=resample)  # Missing areas are padded with 0 !
        res = np.array(rot_im) + min_ar  # min(ar) becomes min_ar again
    # 3D version : 2D version, layer by layer
    elif ar.ndim == 3:
        mins = np.min(ar, (1, 2))
        ar -= mins[:, None, None]
        ims = [Image.fromarray(ar[z]) for z in range(ar.shape[0])]
        rot_ims = [im.rotate(angle, resample=resample) for im in ims]
        rot_ims = [rot_ims[i] + mins[i] for i in range(len(ims))]
        res = np.stack(rot_ims, axis=2)
    return res


def translate_array(ar, new_center):
    """ Translate an array such as the element located at new_center is sent at the center"""
    center = np.array((ar.shape[0] / 2, ar.shape[1] / 2))
    new_center = np.asarray(new_center)
    trans = -(new_center - center).astype(int)
    ar = np.roll(ar, trans, axis=(0, 1))
    return ar
