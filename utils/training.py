import torch.nn.functional as F

def train(train_loader, epoch, network, optimizer, ep=None, train_graph=None):
    network.train()
    train_loss = 0
    for batch_idx, (data, target, m_in, s_in) in enumerate(train_loader):
        optimizer.zero_grad()
        output = network(data)
        loss = F.mse_loss(output, target)
        train_loss += loss.item()
        loss.backward()
        optimizer.step()
        if batch_idx % 10 == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.3e}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                       100. * batch_idx / len(train_loader), loss.item()))
    train_loss /= len(train_loader.dataset)
    print('Train set, Average loss: {:.3e}\n'.format(train_loss))
    if (ep is not None) and (train_graph is not None):
        ep.append(epoch)
        train_graph.append(train_loss)
    return train_loss


def test(test_loader, network, test_graph=None):
    network.eval()
    test_loss = 0
    for (data, target, m_in, s_in) in test_loader:
        output = network(data)
        test_loss += F.mse_loss(output, target).item()  # sum up batch loss
    test_loss /= len(test_loader.dataset)
    print('Validation/Test set, Average loss: {:.3e}\n'.format(test_loss))
    if test_graph is not None:
        test_graph.append(test_loss)
    return test_loss