import errno
import os
import re
from os.path import join

import numpy as np


# Utils for managing files and extracting metadata from their names
def str_to_tuple(s):
    s = s.split(", ")
    s[0] = int(s[0][1:])
    s[1] = int(s[1])
    s[2] = int(s[2][:-1])
    return s


def create_expr(x):
    if x is not None:
        expr = ""
        for e in np.array(x).flatten():
            expr += str(e) + "|"
        return expr[:-1]
    else:
        return ".*"


def name2param(name):
    param = re.search(
        r"(.*)_p(.*)_fs(.*)_a(.*)_iso_(.*)_(.*)_(.*)_n1e(.*)-(.*)", str(name))
    if param:
        param = param.groups()
        param_dict = dict()
        param_dict["anatomy"] = param[0]
        param_dict["patient"] = int(param[1])
        param_dict["field_size"] = int(param[2])
        param_dict["angle"] = int(param[3])
        param_dict["iso"] = (int(param[4]), int(param[5]), int(param[6]))
        param_dict["n_particles"] = 10 ** int(param[7])
        param_dict["type"] = param[8].split(".")[0]
        return param_dict
    else:
        raise NameError(
            "Invalid name : {}\n Impossible to extract information".format(name))


def folder_name(param_dict, root_folder):
    folder = join(root_folder, param_dict["anatomy"])
    folder = join(folder, "patient{}".format(param_dict["patient"]))
    folder = join(folder, "n{}".format(int(param_dict["n_particles"])))
    folder = join(folder, "field_size{}".format(param_dict["field_size"]))
    folder = join(folder, "angle{}".format(param_dict["angle"]))
    if isinstance(param_dict["iso"], tuple):
        folder = join(folder, "iso_{}_{}_{}".format(*param_dict["iso"]))
    elif isinstance(param_dict["iso"], str):
        iso = str_to_tuple(param_dict["iso"])
        folder = join(folder, "iso_{}_{}_{}".format(*iso))
    return folder


def create_directory(directory):
    try:
        os.makedirs(directory)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
