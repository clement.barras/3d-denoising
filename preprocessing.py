# %%

import os
import re

import SimpleITK as sitk
import numpy as np
import pandas as pd
from IPython.core.display import clear_output

from utils import create_directory, name2param, folder_name


def get_image_array(file):
    """
    Extract and convert into arrays .mhd or .mha volume files
    """
    image = sitk.ReadImage(file)
    ary = sitk.GetArrayFromImage(image)
    # Simple ITK images are indexes (x, y, z) while by default numpy arrays are [z, y, x] : transpose is needed
    ary = np.transpose(ary, (2, 1, 0))
    ary = ary[:, :, ::-1]
    ary = (ary - np.min(ary)) / (np.max(ary) - np.min(ary))  # normalize between 0 and 1
    return ary


def volume2array(dose_folder, output_folder, typ="Dose", force_extraction=False):
    """
    Extract each volume of a dose_folder it into 2D arrays and save them under numpy .npy format.
    """
    listdir = os.listdir(dose_folder)
    listdir.sort()
    volume_list = [f for f in listdir if not (os.path.isdir(f))]  # list volumes (files in folder)
    print("Number of files to extract : {}".format(len(volume_list)))
    for i, volume in enumerate(volume_list):
        params = name2param(volume)  # decypher params of the volume from its name
        if params["type"] != typ:  # filter out volumes for which type is different from the requested "typ"
            print("Discarding {} ({}/{}) : type \"{}\" is different from requested type \"{}\"".
                  format(volume, i + 1, len(volume_list), params["type"], typ))
            continue
        ary = get_image_array(os.path.join(dose_folder, volume))  # convert the 3D file into a 3D array
        slices_id = list(range(ary.shape[2]))
        result_dir = folder_name(params, output_folder)
        create_directory(result_dir)
        print("Saving volume in {} ({}/{})".format(result_dir, i + 1, len(volume_list)))
        for z in slices_id:  # save each slice in an individual file
            result_file = os.path.join(result_dir, "slice_#{}".format(z))
            if force_extraction or (result_file not in os.listdir(result_dir)):  # erase an existing file if exists
                np.save(result_file, ary[:, :, z])


def volume2array_patient(patient_folder, output_folder, anatomies=('lung', 'orl', 'prostate')):
    """Same as previous for the patient's CT volumes"""
    listdir = os.listdir(patient_folder)
    listdir.sort()
    for anatomy in anatomies:
        for file in listdir:
            patient = re.search(r"{}_(.*).mha".format(anatomy), str(file))  # find patient images in folder
            if patient:
                patient = int(patient.groups()[0])
                ary = get_image_array(os.path.join(patient_folder, file))  # conversion to array
                slices_id = list(range(ary.shape[2]))
                # creates a directory whose name depends on the patient's
                result_dir = os.path.join(output_folder, anatomy, "patient{}".format(patient), "CT")
                create_directory(result_dir)
                print("Saving volume in {}".format(result_dir))
                for z in slices_id:  # save each slice individually
                    result_file = os.path.join(result_dir, "slice_#{}".format(z))
                    np.save(result_file, ary[:, :, z])


def create_summary_df(extracted_folder, output_folder, typ="Dose"):
    """
    Parse a folder of extracted data (with volume2array) and create a dataframe gathering metadata for each file
    """
    listdir = os.listdir(extracted_folder)
    listdir.sort()
    volume_list = [f for f in listdir if not (os.path.isdir(f))]
    summary_df = pd.DataFrame(columns=  # contains meta data for each slice
                              ["anatomy", "patient", "n_particles", "field_size", "angle", "type", "iso", "slice"])

    for i, volume in enumerate(volume_list):
        clear_output()
        print("Processing volume #{}".format(i + 1))
        params = name2param(volume)
        if params["type"] != typ:  # discard if type is not matching requested type
            continue
        volume_folder = folder_name(params, output_folder)
        for f in os.listdir(volume_folder):
            slice_id = re.search(r"slice_#(.*).npy", str(f))
            if slice_id:
                slice_id = int(slice_id.groups()[0])
                to_append = pd.Series(params)
                to_append["slice"] = slice_id
                summary_df = summary_df.append(to_append, ignore_index=True)  # append each existing slice as a row
    return summary_df


def find_max_energy_window(distr, threshold):
    # Find boundaries indexes for which a distribution of energy is above a given threshold
    axis = np.arange(len(distr))
    filtered_axis = axis[distr >= threshold]
    return filtered_axis[0], filtered_axis[-1]


def calc_bounding_box(ary, thres_ratio=0.10):
    # locate the ray by calculating windows of maximum energy around it
    distr_x = np.sum(ary ** 2, axis=(1, 2))
    thres_x = np.max(distr_x) * thres_ratio
    w1, w2 = find_max_energy_window(distr_x, thres_x)
    distr_y = np.sum(ary ** 2, axis=(0, 2))
    thres_y = np.max(distr_y) * thres_ratio
    l1, l2 = find_max_energy_window(distr_y, thres_y)
    distr_z = np.sum(ary ** 2, axis=(0, 1))
    thres_z = np.max(distr_z) * thres_ratio
    h1, h2 = find_max_energy_window(distr_z, thres_z)
    order = tuple(np.argsort([w2 - w1, l2 - l1, h2 - h1]))
    bbox = {"width": w2 - w1 + 1, "length": l2 - l1 + 1, "height": h2 - h1 + 1,
            "x_min": w1, "x_max": w2, "y_min": l1, "y_max": l2,
            "z_min": h1, "z_max": h2, "order": order}
    return bbox
