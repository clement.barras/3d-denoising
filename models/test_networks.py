import math
from math import sqrt

import torch
import torch.nn as nn
import torch.nn.functional as F


class TestNet(nn.Module):

    def __init__(self, input_nbr, label_nbr):
        super(TestNet, self).__init__()

        self.conv1 = nn.Conv2d(input_nbr, 32, kernel_size=3, padding=3, groups=2, dilation=2)
        self.conv2 = nn.Conv2d(32, 32, kernel_size=3, padding=2, groups=2, dilation=2)
        self.conv3 = nn.Conv2d(32, 32, kernel_size=3, padding=2, groups=2, dilation=2)

        self.linear = nn.Linear(32 * 16 * 16, 32 * 16 * 16, bias=True)

        self.unconv3 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
        self.unconv2 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
        self.unconv1 = nn.Conv2d(32, 32, kernel_size=3, padding=0)

        self.final_conv = nn.Conv2d(32, label_nbr, kernel_size=3, padding=1)

    def forward(self, input):
        """Forward method."""
        print(input.size())
        x = self.conv1(input)

        x = F.relu(x)
        size1 = x.size()
        x, id1 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv2(x)

        x = F.relu(x)
        size2 = x.size()
        x, id2 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv3(x)

        x = F.relu(x)
        size3 = x.size()
        x, id3 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = x.view(-1, 32 * 16 * 16)
        x = self.linear(x)
        x = F.relu(x)
        x = x.view(-1, 32, 16, 16)

        x = F.max_unpool2d(x, id3, kernel_size=2, stride=2, output_size=size3)
        x = self.unconv3(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id2, kernel_size=2, stride=2)
        x = self.unconv2(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id1, kernel_size=2, stride=2)
        x = self.unconv1(x)
        x = F.relu(x)

        x = self.final_conv(x)

        return x


class TestNet11(nn.Module):

    def __init__(self, input_nbr, label_nbr):
        super(TestNet11, self).__init__()

        self.conv1 = nn.Conv2d(input_nbr, 32, kernel_size=3, padding=2, groups=2, dilation=2)
        self.conv2 = nn.Conv2d(32, 32, kernel_size=3, padding=2, groups=2, dilation=2)
        self.conv3 = nn.Conv2d(32, 32, kernel_size=3, padding=2, groups=2, dilation=2)
        self.conv4 = nn.Conv2d(32, 32, kernel_size=3, padding=2, groups=2, dilation=2)
        self.conv5 = nn.Conv2d(32, 32, kernel_size=3, padding=2, groups=2, dilation=2)

        self.linear = nn.Linear(32 * 16 * 16, 32 * 16 * 16, bias=True)

        self.unconv5 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
        self.unconv4 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
        self.unconv3 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
        self.unconv2 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
        self.unconv1 = nn.Conv2d(32, 32, kernel_size=3, padding=1)

        self.final_conv = nn.Conv2d(32, label_nbr, kernel_size=3, padding=1)

    def forward(self, input):
        """Forward method."""

        x = self.conv1(input)
        x = F.relu(x)
        size1 = x.size()
        x, id1 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv2(x)
        x = F.relu(x)
        size2 = x.size()
        x, id2 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv3(x)
        x = F.relu(x)
        size3 = x.size()
        x, id3 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv4(x)
        x = F.relu(x)
        size4 = x.size()
        x, id4 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv5(x)
        x = F.relu(x)
        size5 = x.size()
        x, id5 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = x.view(-1, 32 * 16 * 16)
        x = self.linear(x)
        x = F.relu(x)
        x = x.view(-1, 32, 16, 16)

        x = F.max_unpool2d(x, id5, kernel_size=2, stride=2, output_size=size5)
        x = self.unconv5(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id4, kernel_size=2, stride=2, output_size=size4)
        x = self.unconv4(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id3, kernel_size=2, stride=2, output_size=size3)
        x = self.unconv3(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id2, kernel_size=2, stride=2)
        x = self.unconv2(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id1, kernel_size=2, stride=2)
        x = self.unconv1(x)
        x = F.relu(x)

        x = self.final_conv(x)

        return x


class TestNet2(nn.Module):

    def __init__(self, input_nbr, label_nbr, final_relu=False):
        super(TestNet2, self).__init__()
        self.final_relu = final_relu

        self.conv1 = nn.Conv2d(input_nbr, 64, kernel_size=3, padding=1, groups=2)
        self.conv11 = nn.Conv2d(64, 64, kernel_size=3, padding=1, groups=2)
        self.conv2 = nn.Conv2d(64, 128, kernel_size=3, padding=1, groups=2)
        self.conv22 = nn.Conv2d(128, 128, kernel_size=3, padding=1, groups=2)
        self.conv3 = nn.Conv2d(128, 128, kernel_size=3, padding=1, groups=2)
        self.conv33 = nn.Conv2d(128, 128, kernel_size=3, padding=1, groups=2)

        self.unconv3 = nn.Conv2d(128, 128, kernel_size=3, padding=1)
        self.unconv33 = nn.Conv2d(128, 128, kernel_size=3, padding=1)
        self.unconv2 = nn.Conv2d(128, 128, kernel_size=3, padding=1)
        self.unconv22 = nn.Conv2d(128, 64, kernel_size=3, padding=1)
        self.unconv1 = nn.Conv2d(64, 64, kernel_size=3, padding=1)
        self.unconv11 = nn.Conv2d(64, 64, kernel_size=3, padding=1)

        self.final_conv = nn.Conv2d(64, label_nbr, kernel_size=3, padding=1)

    def forward(self, input):
        """Forward method."""
        # print(input.size())
        x = self.conv1(input)
        x = F.relu(x)
        x = self.conv11(x)
        x = F.relu(x)
        size1 = x.size()
        x, id1 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv2(x)
        x = F.relu(x)
        x = self.conv22(x)
        x = F.relu(x)
        size2 = x.size()
        x, id2 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv3(x)
        x = F.relu(x)
        x = self.conv33(x)
        x = F.relu(x)
        size3 = x.size()
        x, id3 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = F.max_unpool2d(x, id3, kernel_size=2, stride=2, output_size=size3)
        x = self.unconv3(x)
        x = F.relu(x)
        x = self.unconv33(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id2, kernel_size=2, stride=2, output_size=size2)
        x = self.unconv2(x)
        x = F.relu(x)
        x = self.unconv22(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id1, kernel_size=2, stride=2, output_size=size1)
        x = self.unconv1(x)
        x = F.relu(x)
        x = self.unconv11(x)
        x = F.relu(x)

        x = self.final_conv(x)
        if self.final_relu:
            x = F.relu(x)
        # print(x.size())
        return x


class TestNet23(nn.Module):

    def __init__(self, input_nbr, label_nbr, final_relu=False):
        super(TestNet23, self).__init__()
        self.final_relu = final_relu

        self.conv1 = nn.Conv2d(input_nbr, 16, kernel_size=3, padding=1, groups=2)
        self.conv11 = nn.Conv2d(16, 32, kernel_size=3, padding=1, groups=2)
        self.conv2 = nn.Conv2d(32, 32, kernel_size=3, padding=1, groups=2)
        self.conv22 = nn.Conv2d(32, 32, kernel_size=3, padding=1, groups=2)
        self.conv3 = nn.Conv2d(32, 64, kernel_size=3, padding=1, groups=2)
        self.conv33 = nn.Conv2d(64, 64, kernel_size=3, padding=1, groups=2)

        self.unconv3 = nn.Conv2d(64, 64, kernel_size=3, padding=1)
        self.unconv33 = nn.Conv2d(64, 32, kernel_size=3, padding=1)
        self.unconv2 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
        self.unconv22 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
        self.unconv1 = nn.Conv2d(32, 16, kernel_size=3, padding=1)
        self.unconv11 = nn.Conv2d(16, 16, kernel_size=3, padding=1)

        self.final_conv = nn.Conv2d(16, label_nbr, kernel_size=3, padding=1)

    def forward(self, input):
        """Forward method."""
        # print(input.size())
        x = self.conv1(input)
        x = F.relu(x)
        x = self.conv11(x)
        x = F.relu(x)
        size1 = x.size()
        x, id1 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv2(x)
        x = F.relu(x)
        x = self.conv22(x)
        x = F.relu(x)
        size2 = x.size()
        x, id2 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv3(x)
        x = F.relu(x)
        x = self.conv33(x)
        x = F.relu(x)
        size3 = x.size()
        x, id3 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = F.max_unpool2d(x, id3, kernel_size=2, stride=2, output_size=size3)
        x = self.unconv3(x)
        x = F.relu(x)
        x = self.unconv33(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id2, kernel_size=2, stride=2, output_size=size2)
        x = self.unconv2(x)
        x = F.relu(x)
        x = self.unconv22(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id1, kernel_size=2, stride=2, output_size=size1)
        x = self.unconv1(x)
        x = F.relu(x)
        x = self.unconv11(x)
        x = F.relu(x)

        x = self.final_conv(x)
        if self.final_relu:
            x = F.relu(x)
        # print(x.size())
        return x


class TestNet22(nn.Module):

    def __init__(self, input_nbr, label_nbr, final_relu=False):
        super(TestNet22, self).__init__()
        self.final_relu = final_relu

        self.conv1 = nn.Conv2d(input_nbr, 16, kernel_size=3, padding=1, groups=2)
        self.conv11 = nn.Conv2d(16, 16, kernel_size=3, padding=1, groups=2)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=3, padding=1, groups=2)
        self.conv22 = nn.Conv2d(32, 32, kernel_size=3, padding=1, groups=2)
        self.conv3 = nn.Conv2d(32, 32, kernel_size=3, padding=1, groups=2)
        self.conv33 = nn.Conv2d(32, 32, kernel_size=3, padding=1, groups=2)
        self.conv4 = nn.Conv2d(32, 64, kernel_size=3, padding=1, groups=2)
        self.conv44 = nn.Conv2d(64, 64, kernel_size=3, padding=1, groups=2)

        self.unconv4 = nn.Conv2d(64, 64, kernel_size=3, padding=1)
        self.unconv44 = nn.Conv2d(64, 32, kernel_size=3, padding=1)
        self.unconv3 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
        self.unconv33 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
        self.unconv2 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
        self.unconv22 = nn.Conv2d(32, 16, kernel_size=3, padding=1)
        self.unconv1 = nn.Conv2d(16, 16, kernel_size=3, padding=1)
        self.unconv11 = nn.Conv2d(16, 16, kernel_size=3, padding=1)

        self.final_conv = nn.Conv2d(16, label_nbr, kernel_size=3, padding=1)

    def forward(self, input):
        """Forward method."""
        # print(input.size())
        x = self.conv1(input)
        x = F.relu(x)
        x = self.conv11(x)
        x = F.relu(x)
        size1 = x.size()
        x, id1 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv2(x)
        x = F.relu(x)
        x = self.conv22(x)
        x = F.relu(x)
        size2 = x.size()
        x, id2 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv3(x)
        x = F.relu(x)
        x = self.conv33(x)
        x = F.relu(x)
        size3 = x.size()
        x, id3 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv4(x)
        x = F.relu(x)
        x = self.conv44(x)
        x = F.relu(x)
        size4 = x.size()
        x, id4 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = F.max_unpool2d(x, id4, kernel_size=2, stride=2, output_size=size4)
        x = self.unconv4(x)
        x = F.relu(x)
        x = self.unconv44(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id3, kernel_size=2, stride=2, output_size=size3)
        x = self.unconv3(x)
        x = F.relu(x)
        x = self.unconv33(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id2, kernel_size=2, stride=2, output_size=size2)
        x = self.unconv2(x)
        x = F.relu(x)
        x = self.unconv22(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id1, kernel_size=2, stride=2, output_size=size1)
        x = self.unconv1(x)
        x = F.relu(x)
        x = self.unconv11(x)
        x = F.relu(x)

        x = self.final_conv(x)
        if self.final_relu:
            x = F.relu(x)
        # print(x.size())
        return x


class TestNet3(nn.Module):

    def __init__(self, input_nbr, label_nbr):
        super(TestNet3, self).__init__()

        self.conv1 = nn.Conv2d(input_nbr, 32, kernel_size=3, padding=1, groups=2)
        self.conv2 = nn.Conv2d(32, 32, kernel_size=3, padding=1, groups=2)
        self.conv3 = nn.Conv2d(32, 32, kernel_size=3, padding=1, groups=2)

        self.linear = nn.Linear(32 * 15 * 15, 32 * 15 * 15, bias=True)

        self.unconv3 = nn.ConvTranspose2d(32, 32, kernel_size=3, padding=1)
        self.unconv2 = nn.ConvTranspose2d(32, 32, kernel_size=3, padding=1)
        self.unconv1 = nn.ConvTranspose2d(32, 32, kernel_size=3, padding=1)

        self.final_conv = nn.Conv2d(32, label_nbr, kernel_size=3, padding=1)

    def forward(self, input):
        """Forward method."""

        x = self.conv1(input)
        x = F.relu(x)
        size1 = x.size()
        x, id1 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv2(x)
        x = F.relu(x)
        size2 = x.size()
        x, id2 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv3(x)
        x = F.relu(x)
        size3 = x.size()
        x, id3 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)
        # print(x.size())

        x = x.view(-1, 32 * 15 * 15)
        x = self.linear(x)
        x = F.relu(x)
        x = x.view(-1, 32, 15, 15)

        x = F.max_unpool2d(x, id3, kernel_size=2, stride=2, output_size=size3)
        x = self.unconv3(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id2, kernel_size=2, stride=2, output_size=size2)
        x = self.unconv2(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id1, kernel_size=2, stride=2, output_size=size1)
        x = self.unconv1(x)
        x = F.relu(x)

        x = self.final_conv(x)
        return x


class TestNet4(nn.Module):

    def __init__(self, input_nbr, label_nbr):
        super(TestNet4, self).__init__()

        self.conv1 = nn.Conv2d(input_nbr, 32, kernel_size=3, padding=1, groups=2)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=3, padding=1, groups=2)
        self.conv3 = nn.Conv2d(64, 64, kernel_size=3, padding=1, groups=2)

        self.linear = nn.Linear(64 * 15 * 15, 64 * 15 * 15, bias=True)

        self.unconv3 = nn.Conv2d(64, 64, kernel_size=3, padding=1)
        self.unconv2 = nn.Conv2d(64, 32, kernel_size=3, padding=1)
        self.unconv1 = nn.Conv2d(32, 32, kernel_size=3, padding=1)

        self.final_conv = nn.Conv2d(32, label_nbr, kernel_size=3, padding=1)

    def forward(self, input):
        """Forward method."""

        x = self.conv1(input)
        x = F.relu(x)
        size1 = x.size()
        x, id1 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv2(x)
        x = F.relu(x)
        size2 = x.size()
        x, id2 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv3(x)
        x = F.relu(x)
        size3 = x.size()
        x, id3 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)
        # print(x.size())

        x = x.view(-1, 64 * 15 * 15)
        x = self.linear(x)
        x = F.relu(x)
        x = x.view(-1, 64, 15, 15)

        x = F.max_unpool2d(x, id3, kernel_size=2, stride=2, output_size=size3)
        x = self.unconv3(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id2, kernel_size=2, stride=2, output_size=size2)
        x = self.unconv2(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id1, kernel_size=2, stride=2, output_size=size1)
        x = self.unconv1(x)
        x = F.relu(x)

        x = self.final_conv(x)
        return x


class DnCNN(nn.Module):
    def __init__(self, channels, out_channels=1, num_of_layers=17):
        super(DnCNN, self).__init__()
        kernel_size = 3
        padding = 1
        features = 64
        layers = []
        layers.append(
            nn.Conv2d(in_channels=channels, out_channels=features, kernel_size=kernel_size, padding=padding, groups=2,
                      bias=False))
        layers.append(nn.ReLU(inplace=True))
        for _ in range(7):
            layers.append(
                nn.Conv2d(in_channels=features, out_channels=features, kernel_size=kernel_size, padding=padding,
                          groups=2, bias=False))
            # layers.append(nn.BatchNorm2d(features))
            layers.append(nn.ReLU(inplace=True))
        for _ in range(num_of_layers - 9):
            layers.append(
                nn.Conv2d(in_channels=features, out_channels=features, kernel_size=kernel_size, padding=padding,
                          bias=False))
            # layers.append(nn.BatchNorm2d(features))
            layers.append(nn.ReLU(inplace=True))
        layers.append(
            nn.Conv2d(in_channels=features, out_channels=out_channels, kernel_size=kernel_size, padding=padding,
                      bias=False))
        self.dncnn = nn.Sequential(*layers)

    def forward(self, x):
        out = self.dncnn(x)
        return out


class UNet(nn.Module):
    """Custom U-Net architecture for Noise2Noise (see Appendix, Table 2)."""

    def __init__(self, in_channels=2, out_channels=1):
        """Initializes U-Net."""

        super(UNet, self).__init__()

        # Layers: enc_conv0, enc_conv1, pool1
        self._block1 = nn.Sequential(
            nn.Conv2d(in_channels, 48, 3, stride=1, padding=1, groups=2),
            nn.ReLU(inplace=True),
            nn.Conv2d(48, 48, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(2))

        # Layers: enc_conv(i), pool(i); i=2..5
        self._block2 = nn.Sequential(
            nn.Conv2d(48, 48, 3, stride=1, padding=1, groups=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(2))

        # Layers: enc_conv6, upsample5
        self._block3 = nn.Sequential(
            nn.Conv2d(48, 48, 3, stride=1, padding=1, groups=2),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(48, 96, 3, stride=2, padding=1, output_padding=1))
        # nn.Upsample(scale_factor=2, mode='nearest'))

        # Layers: dec_conv5a, dec_conv5b, upsample4
        self._block4 = nn.Sequential(
            nn.Conv2d(96, 96, 3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(96, 96, 3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(96, 96, 3, stride=2, padding=1, output_padding=1))
        # nn.Upsample(scale_factor=2, mode='nearest'))

        # Layers: dec_deconv(i)a, dec_deconv(i)b, upsample(i-1); i=4..2
        self._block5 = nn.Sequential(
            nn.Conv2d(96, 96, 3, stride=1, padding=2),
            nn.ReLU(inplace=True),
            nn.Conv2d(96, 96, 3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(96, 96, 3, stride=2, padding=1, output_padding=1))
        # nn.Upsample(scale_factor=2, mode='nearest'))

        # Layers: dec_conv1a, dec_conv1b, dec_conv1c,
        self._block6 = nn.Sequential(
            nn.Conv2d(96, 64, 3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(64, 32, 3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(32, out_channels, 3, stride=1, padding=2),
            nn.LeakyReLU(0.1))

        # Initialize weights
        self._init_weights()

    def _init_weights(self):
        """Initializes weights using He et al. (2015)."""

        for m in self.modules():
            if isinstance(m, nn.ConvTranspose2d) or isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.data)
                m.bias.data.zero_()

    def forward(self, x):
        """Through encoder, then decoder by adding U-skip connections. """

        # Encoder
        pool1 = self._block1(x)
        pool2 = self._block2(pool1)
        pool3 = self._block2(pool2)
        pool4 = self._block2(pool3)
        pool5 = self._block2(pool4)

        # Decoder
        upsample5 = self._block3(pool5)
        # concat5 = torch.cat((upsample5, pool4), dim=1)
        upsample4 = self._block4(upsample5)
        # concat4 = torch.cat((upsample4, pool3), dim=1)
        upsample3 = self._block5(upsample4)
        # concat3 = torch.cat((upsample3, pool2), dim=1)
        upsample2 = self._block5(upsample3)
        # concat2 = torch.cat((upsample2, pool1), dim=1)
        upsample1 = self._block5(upsample2)
        # concat1 = torch.cat((upsample1, x), dim=1)

        # Final activation
        return self._block6(upsample1)


def default_conv(in_channels, out_channels, kernel_size, bias=True):
    return nn.Conv2d(
        in_channels, out_channels, kernel_size,
        padding=(kernel_size // 2), bias=bias)


class MeanShift(nn.Conv2d):
    def __init__(
            self, rgb_range,
            rgb_mean=(0.4488, 0.4371, 0.4040), rgb_std=(1.0, 1.0, 1.0), sign=-1):
        super(MeanShift, self).__init__(3, 3, kernel_size=1)
        std = torch.Tensor(rgb_std)
        self.weight.data = torch.eye(3).view(3, 3, 1, 1) / std.view(3, 1, 1, 1)
        self.bias.data = sign * rgb_range * torch.Tensor(rgb_mean) / std
        for p in self.parameters():
            p.requires_grad = False


class BasicBlock(nn.Sequential):
    def __init__(
            self, conv, in_channels, out_channels, kernel_size, stride=1, bias=False,
            bn=True, act=nn.ReLU(True)):

        m = [conv(in_channels, out_channels, kernel_size, bias=bias)]
        if bn:
            m.append(nn.BatchNorm2d(out_channels))
        if act is not None:
            m.append(act)

        super(BasicBlock, self).__init__(*m)


class ResBlock(nn.Module):
    def __init__(
            self, conv, n_feats, kernel_size,
            bias=True, bn=False, act=nn.ReLU(True), res_scale=1):

        super(ResBlock, self).__init__()
        m = []
        for i in range(2):
            m.append(conv(n_feats, n_feats, kernel_size, bias=bias))
            if bn:
                m.append(nn.BatchNorm2d(n_feats))
            if i == 0:
                m.append(act)

        self.body = nn.Sequential(*m)
        self.res_scale = res_scale

    def forward(self, x):
        res = self.body(x).mul(self.res_scale)
        res += x

        return res


class Upsampler(nn.Sequential):
    def __init__(self, conv, scale, n_feats, bn=False, act=False, bias=True):

        m = []
        if (scale & (scale - 1)) == 0:  # Is scale = 2^n?
            for _ in range(int(math.log(scale, 2))):
                m.append(conv(n_feats, 4 * n_feats, 3, bias))
                m.append(nn.PixelShuffle(2))
                if bn:
                    m.append(nn.BatchNorm2d(n_feats))
                if act == 'relu':
                    m.append(nn.ReLU(True))
                elif act == 'prelu':
                    m.append(nn.PReLU(n_feats))

        elif scale == 3:
            m.append(conv(n_feats, 9 * n_feats, 3, bias))
            m.append(nn.PixelShuffle(3))
            if bn:
                m.append(nn.BatchNorm2d(n_feats))
            if act == 'relu':
                m.append(nn.ReLU(True))
            elif act == 'prelu':
                m.append(nn.PReLU(n_feats))
        else:
            raise NotImplementedError

        super(Upsampler, self).__init__(*m)


def make_model(parent=False):
    return RDN()


class RDB_Conv(nn.Module):
    def __init__(self, inChannels, growRate, kSize=3):
        super(RDB_Conv, self).__init__()
        Cin = inChannels
        G = growRate
        self.conv = nn.Sequential(*[
            nn.Conv2d(Cin, G, kSize, padding=(kSize - 1) // 2, stride=1),
            nn.ReLU()
        ])

    def forward(self, x):
        out = self.conv(x)
        return torch.cat((x, out), 1)


class RDB(nn.Module):
    def __init__(self, growRate0, growRate, nConvLayers, kSize=3):
        super(RDB, self).__init__()
        G0 = growRate0
        G = growRate
        C = nConvLayers

        convs = []
        for c in range(C):
            convs.append(RDB_Conv(G0 + c * G, G))
        self.convs = nn.Sequential(*convs)

        # Local Feature Fusion
        self.LFF = nn.Conv2d(G0 + C * G, G0, 1, padding=0, stride=1)

    def forward(self, x):
        return self.LFF(self.convs(x)) + x


class RDN(nn.Module):
    def __init__(self):
        super(RDN, self).__init__()
        r = 4
        G0 = 64
        kSize = 3

        # number of RDB blocks, conv layers, out channels
        self.D, C, G = (16, 8, 64)

        # Shallow feature extraction net
        self.SFENet1 = nn.Conv2d(2, G0, kSize, padding=(kSize - 1) // 2, stride=1, groups=2)
        self.SFENet2 = nn.Conv2d(G0, G0, kSize, padding=(kSize - 1) // 2, stride=1, groups=2)

        # Redidual dense blocks and dense feature fusion
        self.RDBs = nn.ModuleList()
        for i in range(self.D):
            self.RDBs.append(
                RDB(growRate0=G0, growRate=G, nConvLayers=C)
            )

        # Global Feature Fusion
        self.GFF = nn.Sequential(*[
            nn.Conv2d(self.D * G0, G0, 1, padding=0, stride=1),
            nn.Conv2d(G0, G0, kSize, padding=(kSize - 1) // 2, stride=1)
        ])

        # Up-sampling net
        if r == 2 or r == 3:
            self.UPNet = nn.Sequential(*[
                nn.Conv2d(G0, G * r * r, kSize, padding=(kSize - 1) // 2, stride=1),
                nn.PixelShuffle(r),
                nn.Conv2d(G, 1, kSize, padding=(kSize - 1) // 2, stride=1)
            ])
        elif r == 4:
            self.UPNet = nn.Sequential(*[
                nn.Conv2d(G0, G * 4, kSize, padding=(kSize - 1) // 2, stride=1),
                nn.PixelShuffle(2),
                nn.Conv2d(G, G * 4, kSize, padding=(kSize - 1) // 2, stride=1),
                nn.PixelShuffle(2),
                nn.Conv2d(G, 1, kSize, padding=(kSize - 1) // 2, stride=1)
            ])
        else:
            raise ValueError("scale must be 2 or 3 or 4.")

    def forward(self, x):
        f__1 = self.SFENet1(x)
        x = self.SFENet2(f__1)

        RDBs_out = []
        for i in range(self.D):
            x = self.RDBs[i](x)
            RDBs_out.append(x)

        x = self.GFF(torch.cat(RDBs_out, 1))
        x += f__1

        return self.UPNet(x)


class MemNet(nn.Module):
    def __init__(self, in_channels=2, channels=64, num_memblock=6, num_resblock=6):
        super(MemNet, self).__init__()
        self.feature_extractor = ConvBlocks()
        self.reconstructor = BNReLUConv(channels, 1)
        self.dense_memory = nn.ModuleList(
            [MemoryBlock(channels, num_resblock, i + 1) for i in range(num_memblock)]
        )

    def forward(self, x):
        # x = x.contiguous()

        residual = x[:, 0, :, :]
        out = self.feature_extractor(x)
        ys = [out]
        for memory_block in self.dense_memory:
            out = memory_block(out, ys)
        out = self.reconstructor(out)
        out = out + residual[:, None, :, :]
        return out


class MemoryBlock(nn.Module):
    """Note: num_memblock denotes the number of MemoryBlock currently"""

    def __init__(self, channels, num_resblock, num_memblock):
        super(MemoryBlock, self).__init__()
        self.recursive_unit = nn.ModuleList(
            [ResidualBlock(channels) for i in range(num_resblock)]
        )
        self.gate_unit = BNReLUConv((num_resblock + num_memblock) * channels, channels, 1, 1, 0)

    def forward(self, x, ys):
        """ys is a list which contains long-term memory coming from previous memory block
        xs denotes the short-term memory coming from recursive unit
        """
        xs = []
        residual = x
        for layer in self.recursive_unit:
            x = layer(x)
            xs.append(x)

        gate_out = self.gate_unit(torch.cat(xs + ys, 1))
        ys.append(gate_out)
        return gate_out


class ResidualBlock(torch.nn.Module):
    """ResidualBlock
    introduced in: https://arxiv.org/abs/1512.03385
    x - Relu - Conv - Relu - Conv - x
    """

    def __init__(self, channels, k=3, s=1, p=1):
        super(ResidualBlock, self).__init__()
        self.relu_conv1 = BNReLUConv(channels, channels, k, s, p)
        self.relu_conv2 = BNReLUConv(channels, channels, k, s, p)

    def forward(self, x):
        residual = x
        out = self.relu_conv1(x)
        out = self.relu_conv2(out)
        out = out + residual
        return out


class BNReLUConv(nn.Sequential):
    def __init__(self, in_channels, channels, k=3, s=1, p=1, inplace=True):
        super(BNReLUConv, self).__init__()
        # self.add_module('bn', nn.BatchNorm2d(in_channels))
        self.add_module('relu', nn.ReLU(inplace=inplace))
        self.add_module('conv', nn.Conv2d(in_channels, channels, k, s, p, bias=False))


class ConvBlocks(nn.Module):
    def __init__(self):
        super(ConvBlocks, self).__init__()
        self.conv1 = nn.Conv2d(in_channels=2, out_channels=64, kernel_size=3, stride=1, groups=2, padding=1)
        self.conv2 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, groups=2, padding=1)
        self.conv3 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, padding=1)
        self.relu = nn.ReLU()

    def forward(self, x):
        x = self.relu(self.conv1(x))
        x = self.relu(self.conv2(x))
        out = self.relu(self.conv3(x))
        return out


class Conv_ReLU_Block(nn.Module):
    def __init__(self):
        super(Conv_ReLU_Block, self).__init__()
        self.conv = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, padding=1, bias=False)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        return self.relu(self.conv(x))


class VDSR(nn.Module):
    def __init__(self):
        super(VDSR, self).__init__()
        self.residual_layer = self.make_layer(Conv_ReLU_Block, 18)
        self.input1 = nn.Conv2d(in_channels=2, out_channels=64, kernel_size=3, stride=1, padding=1, bias=False,
                                groups=2)
        self.input2 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, padding=1, bias=False)
        self.output = nn.Conv2d(in_channels=64, out_channels=1, kernel_size=3, stride=1, padding=1, bias=False)
        self.relu = nn.ReLU(inplace=True)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, sqrt(2. / n))

    def make_layer(self, block, num_of_layer):
        layers = []
        for _ in range(num_of_layer):
            layers.append(block())
        return nn.Sequential(*layers)

    def forward(self, x):
        residual = x[:, 0, :, :]
        out = self.relu(self.input1(x))
        out = self.relu(self.input2(out))
        out = self.residual_layer(out)
        out = self.output(out)
        out = torch.add(out, residual[:, None, :, :])
        return out
