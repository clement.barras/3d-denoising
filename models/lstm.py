import torch
import torch.nn as nn
import torch.nn.functional as F


class Encoder(nn.Module):

    def __init__(self, channels=2):
        super(Encoder, self).__init__()

        kernel_size = 3
        padding = 1
        features = 64
        layers = []
        layers.append(
            nn.Conv2d(in_channels=channels, out_channels=features, kernel_size=kernel_size, padding=padding, groups=2,
                      bias=False))
        layers.append(nn.ReLU(inplace=True))
        for _ in range(7):
            layers.append(
                nn.Conv2d(in_channels=features, out_channels=features, kernel_size=kernel_size, padding=padding,
                          groups=2, bias=False))
            # layers.append(nn.BatchNorm2d(features))
            layers.append(nn.ReLU(inplace=True))
        self.encoder = nn.Sequential(*layers)

    def forward(self, x):
        """Forward method."""
        out = self.encoder(x)
        return out


class Decoder(nn.Module):

    def __init__(self, out_channels=1):
        super(Decoder, self).__init__()
        kernel_size = 3
        padding = 1
        features = 64
        layers = []
        for _ in range(8):
            layers.append(
                nn.Conv2d(in_channels=features, out_channels=features, kernel_size=kernel_size, padding=padding,
                          bias=False))
            # layers.append(nn.BatchNorm2d(features))
            layers.append(nn.ReLU(inplace=True))
        layers.append(
            nn.Conv2d(in_channels=features, out_channels=out_channels, kernel_size=kernel_size, padding=padding,
                      bias=False))
        self.decoder = nn.Sequential(*layers)

    def forward(self, x):
        out = self.decoder(x)
        return out


class TestNet22Encoder(nn.Module):

    def __init__(self, input_nbr, final_relu=False):
        super(TestNet22Encoder, self).__init__()
        self.final_relu = final_relu

        self.conv1 = nn.Conv2d(input_nbr, 16, kernel_size=3, padding=1, groups=1)
        self.conv11 = nn.Conv2d(16, 16, kernel_size=3, padding=1, groups=1)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=3, padding=1, groups=1)
        self.conv22 = nn.Conv2d(32, 32, kernel_size=3, padding=1, groups=1)
        self.conv3 = nn.Conv2d(32, 32, kernel_size=3, padding=1, groups=1)
        self.conv33 = nn.Conv2d(32, 32, kernel_size=3, padding=1, groups=1)
        self.conv4 = nn.Conv2d(32, 64, kernel_size=3, padding=1, groups=1)
        self.conv44 = nn.Conv2d(64, 64, kernel_size=3, padding=1, groups=1)

    def forward(self, input):
        """Forward method."""
        # print(input.size())
        x = self.conv1(input)
        x = F.relu(x)
        x = self.conv11(x)
        x = F.relu(x)
        size1 = x.size()
        x, id1 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv2(x)
        x = F.relu(x)
        x = self.conv22(x)
        x = F.relu(x)
        size2 = x.size()
        x, id2 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv3(x)
        x = F.relu(x)
        x = self.conv33(x)
        x = F.relu(x)
        size3 = x.size()
        x, id3 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        x = self.conv4(x)
        x = F.relu(x)
        x = self.conv44(x)
        x = F.relu(x)
        size4 = x.size()
        x, id4 = F.max_pool2d(x, kernel_size=2, stride=2, return_indices=True)

        return x, id1, id2, id3, id4, size1, size2, size3, size4


class TestNet22Decoder(nn.Module):

    def __init__(self, label_nbr, final_relu=False):
        super(TestNet22Decoder, self).__init__()
        self.final_relu = final_relu

        self.unconv4 = nn.Conv2d(64, 64, kernel_size=3, padding=1)
        self.unconv44 = nn.Conv2d(64, 32, kernel_size=3, padding=1)
        self.unconv3 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
        self.unconv33 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
        self.unconv2 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
        self.unconv22 = nn.Conv2d(32, 16, kernel_size=3, padding=1)
        self.unconv1 = nn.Conv2d(16, 16, kernel_size=3, padding=1)
        self.unconv11 = nn.Conv2d(16, 16, kernel_size=3, padding=1)

        self.final_conv = nn.Conv2d(16, label_nbr, kernel_size=3, padding=1)

    def forward(self, x, id1, id2, id3, id4, size1, size2, size3, size4):
        x = F.max_unpool2d(x, id4, kernel_size=2, stride=2, output_size=size4)
        x = self.unconv4(x)
        x = F.relu(x)
        x = self.unconv44(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id3, kernel_size=2, stride=2, output_size=size3)
        x = self.unconv3(x)
        x = F.relu(x)
        x = self.unconv33(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id2, kernel_size=2, stride=2, output_size=size2)
        x = self.unconv2(x)
        x = F.relu(x)
        x = self.unconv22(x)
        x = F.relu(x)

        x = F.max_unpool2d(x, id1, kernel_size=2, stride=2, output_size=size1)
        x = self.unconv1(x)
        x = F.relu(x)
        x = self.unconv11(x)
        x = F.relu(x)

        x = self.final_conv(x)
        if self.final_relu:
            x = F.relu(x)
        # print(x.size())
        return x


class LSTM_MC(nn.Module):
    def __init__(self, channels=2, out_channels=1, embedding_dim=512, hidden_dim=1, num_layers=1, batch_size=8,
                 output_dim=65536):
        super(LSTM_MC, self).__init__()
        self.encoder = TestNet22Encoder(channels)
        self.decoder = TestNet22Decoder(out_channels)
        self.lstm = nn.LSTM(embedding_dim, hidden_dim, num_layers)
        self.batch_size = batch_size
        self.num_layers = num_layers
        self.hidden_dim = hidden_dim
        self.fc = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        batch_size = x.size()[0]
        x = x.permute(1, 0, 2, 3, 4)
        outputs = []
        for i in range(4):
            out, id1, id2, id3, id4, size1, size2, size3, size4 = self.encoder(x[i])
            size = out.size()
            outputs.append(out.view(batch_size, -1))

        out, hidden = self.lstm(torch.stack(outputs).view(4, batch_size, -1))

        out = self.fc(out[-1].view(batch_size, -1))
        out = out.view(size)

        out = self.decoder(out, id1, id2, id3, id4, size1, size2, size3, size4)
        return out
